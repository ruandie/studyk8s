# 预习课程

### 预习说明

有两个文档《docker基础》和《Kubernetes安装手册（非高可用版）》。

- 若之前没有docker的使用经验，建议学习一下docker基础课程
- 若之前有了解docker，想进一步了解k8s，可以尝试按照《安装手册》，尝试提前安装一下k8s集群



### 课前需要准备好的内容

为了保证上课时候不会出现千奇百怪的问题，大家做完上面的练习后，最好恢复机器最初状态，上课的时候再跟随课堂老师一起来操作。

同时，因为课堂的操作中有很多资源是需要在线下载的，因此，为保障不影响课程时间，请大家提前在各节点下载好如下镜像，操作如下：

1. 各节点安装docker

   ```powershell
   ## 配置网卡转发,看值是否为1
   $ sysctl -a |grep -w net.ipv4.ip_forward
   net.ipv4.ip_forward = 1
   
   ## 若未配置，需要执行如下
   $ cat <<EOF >  /etc/sysctl.d/docker.conf
   net.bridge.bridge-nf-call-ip6tables = 1
   net.bridge.bridge-nf-call-iptables = 1
   net.ipv4.ip_forward=1
   EOF
   $ sysctl -p /etc/sysctl.d/docker.conf
   
   ## 下载阿里源repo文件
   $ curl -o /etc/yum.repos.d/Centos-7.repo http://mirrors.aliyun.com/repo/Centos-7.repo
   $ curl -o /etc/yum.repos.d/docker-ce.repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
   
   ## yum安装
   $ yum install -y docker-ce-18.09.9
   
   ## 配置源加速
   $ mkdir -p /etc/docker
   $ vi /etc/docker/daemon.json
   {
     "registry-mirrors" : [
       "https://dockerhub.azk8s.cn",
       "https://reg-mirror.qiniu.com",
       "https://registry.docker-cn.com",
       "https://ot2k4d59.mirror.aliyuncs.com/"
     ]
   }
   
   ## 设置开机自启
   $ systemctl enable docker  
   $ systemctl daemon-reload
   
   ## 启动docker
   $ systemctl start docker 
   ```

2. 下载镜像

   使用docker pull的方式，下载如下镜像到各节点

   ```powershell
   docker pull registry.aliyuncs.com/google_containers/kube-apiserver:v1.16.2
   docker pull registry.aliyuncs.com/google_containers/kube-controller-manager:v1.16.2
   docker pull registry.aliyuncs.com/google_containers/kube-scheduler:v1.16.2
   docker pull registry.aliyuncs.com/google_containers/kube-proxy:v1.16.2
   docker pull registry.aliyuncs.com/google_containers/pause:3.1
   docker pull registry.aliyuncs.com/google_containers/etcd:3.3.15-0
   docker pull registry.aliyuncs.com/google_containers/coredns:1.6.2
   quay.io/coreos/flannel:v0.11.0-amd64
   nginx:alpine
   kubernetesui/dashboard:v2.0.0-beta5
   kubernetesui/metrics-scraper:v1.0.1
   centos:centos7.5.1804
   mysql:5.7
   quay.io/kubernetes-ingress-controller/nginx-ingress-controller:0.28.0
   ```

   



